<p align="center"><img src='https://github.com/oyilmaztekin/vscode-undoc/blob/dev/icon.png' width="200"></p>
<h2 align="center">vscode-undoc</h2>
<p align="center">A document generator for Function Expressions, Function Statements, Arrow Function Expressions and Class Definitions. It detects undocumented functions and class methods in your Javascript / Flow / React code. It collects passed parameters, return expressions, states for React code then creates document template just above the functions.</p>

## Usage

##### Using Command Palette (CMD/CTRL + Shift + P)

`CMD + Shift + P -> Create Docs`

## Install
[undoc](https://marketplace.visualstudio.com/items?itemName=undoc.undoc)

#### React Example
![react-example](/assets/react-example.gif)

#### Flow Example
![flow-example](/assets/flow-example.gif)

## Supports

 1. ES6
 2. Presets of **React**
 3. Presets of **Flow**
 4. States ( only initialized in the constructor )

## Contribution
Feel free to open issues and PRs. You can look [here](https://github.com/oyilmaztekin/vscode-undoc/blob/master/CONTRIBUTING.md) for more details.

1. Fork it (https://github.com/oyilmaztekin/vscode-undoc/fork)
2. Create your branch (`git checkout -b fix/issue-id`)
3. Commit your changes (`git commit -am 'Fixed something...'`)
4. Push the branch (`git push origin fix/issue-id`)
5. Create a new Pull Request

##### TODO
1. Cursor Interaction
2. Currying function
3. PropTypes

## Thanks

* [babel](https://github.com/babel)
